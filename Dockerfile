FROM grafana/grafana:4.3.1

# Needed by the supervisor
RUN mkdir -p /var/log/supervisor/

# Update/Install O.S. dependencies
RUN apt-get -y update && \
    apt-get -y install python-pip && \
    apt-get -y install wget
RUN pip install supervisor
RUN chmod 755 /run.sh

# Install Prometheus
RUN mkdir prometheus
RUN wget -O prometheus/prometheus.tar.gz https://github.com/prometheus/prometheus/releases/download/v1.6.3/prometheus-1.6.3.linux-amd64.tar.gz
RUN tar xvfz prometheus/prometheus.tar.gz -C prometheus/
RUN mv prometheus/prometheus-* prometheus/prometheus-dir

# Grafana automation
RUN mkdir /sources
RUN mkdir /dashboards
RUN mkdir /users
ADD ./create-all.sh /

ADD ./supervisord.conf /etc/supervisor/supervisord.conf
ENTRYPOINT ["supervisord"]
