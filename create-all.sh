#!/bin/bash
# sleeping for 10 seconds by default to let grafana get up and running

WAIT_TO_LOAD=${WAITING_TIME:-10}
GRAFANA_PORT=${GF_SERVER_HTTP_PORT:-3000}

for file in /sources/*
do
  echo ""
  echo "Waiting $WAIT_TO_LOAD seconds to load $file"
  sleep $WAIT_TO_LOAD && curl -X POST -H 'Content-Type: application/json;charset=UTF-8' -H "Accept: application/json" -d @$file http://admin:admin@localhost:$GRAFANA_PORT/api/datasources
done

for file in /dashboards/*
do
  echo ""
  echo "Waiting $WAIT_TO_LOAD seconds to load $file"
  sleep $WAIT_TO_LOAD && curl -X POST -H "Content-Type: application/json;charset=UTF-8" -H "Accept: application/json" -d @$file http://admin:admin@localhost:$GRAFANA_PORT/api/dashboards/db
done

for file in /users/*
do
  echo ""
  echo "Waiting $WAIT_TO_LOAD seconds to load $file"
  sleep $WAIT_TO_LOAD && curl -X POST -H "Content-Type: application/json;charset=UTF-8" -H "Accept: application/json" -d @$file http://admin:admin@localhost:$GRAFANA_PORT/api/admin/users
done
